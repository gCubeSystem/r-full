FROM rocker/r-ver:4.2.3

LABEL org.d4science.image.licenses="EUPL-1.2" \
      org.d4science.image.source="https://code-repo.d4science.org/gCubeSystem/r-full" \
      org.d4science.image.vendor="D4Science <https://www.d4science.org>" \
      org.d4science.image.authors="Andrea Dell'Amico <andrea.dellamico@isti.cnr.it>"

ENV DEBIAN_FRONTEND noninteractive
# Upgrade, install some R dependencies and base python3
RUN apt-get update --yes && \
    apt-get dist-upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    python3 \
    python3-pip \
    curl \
    jags \
    libxml2 \
    libxt6 \
    git \
    gdal-bin \
    libgdal-dev \
    libsodium-dev \
    libudunits2-dev \
    libfontconfig1-dev \
    librdf0 \
    librdf0-dev \
    netcdf-bin \
    libcairo2-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    sshpass \
    patch \
    p7zip-full \
    sox \
    imagemagick \
    proj-bin \
    proj-data \
    libgslcblas0 \
    postgresql-server-dev-all \
    libsecret-1-0 \
    fuse \
    zip \
    jq \
    openjdk-8-jdk-headless \
    openjdk-11-jdk-headless

# Install the R packages we need, using the binary distribution
# at https://packagemanager.rstudio.com/client/#/repos/1/overview
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/install2r-packages.txt /tmp
RUN for f in `cat /tmp/install2r-packages.txt` ; do install2.r --error --skipinstalled --ncpus -1 $f || exit 1 ; done

# Non CRAN repositories
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/r_non_cran_pkgs.txt /tmp
RUN for l in `cat /tmp/r_non_cran_pkgs.txt` ; do \
    pkg=`echo $l | cut -d : -f 1` ; \
    mirror=`echo $l | cut -d : -f 2-` ; \
    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
    -e "install.packages(pkgs='$pkg', repos=c('$mirror/'));" || exit 1 ; done

# Install some CRAN packages from source
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/jupyter-image-r-cran-pkgs.txt /tmp
RUN for cr in `cat /tmp/jupyter-image-r-cran-pkgs.txt` ; do \
    pkg=`echo $cr` ; \
    mirror='https://cloud.r-project.org' ; \
    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
    -e "install.packages(pkgs='$pkg', repos=c('$mirror/'));" || exit 1 ; done

# From github
ADD https://code-repo.d4science.org/gCubeSystem/r-packages-list/raw/branch/master/r_github_pkgs.txt /tmp
RUN echo "GITHUB_PAT=$GITHUB_PAT" >> /usr/local/lib/R/etc/Renviron.site
#RUN for g in `cat /tmp/r_github_pkgs.txt` ; do \
#    Rscript --slave --no-site-file --no-init-file --no-save --no-restore-history \
#    -e " require(devtools); devtools::install_github('$g')" ; done
#RUN for g in `cat /tmp/r_github_pkgs.txt` ; do installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest $g ; done
#RUN grep -v "GITHUB_PATH" /etc/R/Renviron.site > /etc/R/Renviron.site.1
#RUN mv -f /etc/R/Renviron.site.1 /etc/R/Renviron.site
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest DanOvando/sraplus
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest sofia-tsaf/SOFIA
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest jabbamodel/JABBA
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest tokami/lbmsmt
RUN installGithub.r -d TRUE -u FALSE -r https://packagemanager.rstudio.com/all/__linux__/focal/latest james-thorson/FishLife

# Ensure that we remove the source packages
RUN rm -fr /tmp/downloaded_packages/*

RUN apt-get clean && rm -rf /var/lib/apt/lists/* && \
    rm -f /tmp/R* /tmp/install2r-packages.txt /tmp/r_github_pkgs.txt /tmp/jupyter-image-r-cran-pkgs.txt /tmp/r_non_cran_pkgs.txt && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen
